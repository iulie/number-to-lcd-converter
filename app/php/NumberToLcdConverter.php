<?php

namespace App;

class NumberToLcdConverter
{
    private int $width;
    private int $height;

    public function __construct(int $width = 1, int $height = 1)
    {
        $this->width = $width;
        $this->height = $height;
    }

    private const LCDCHARSOFDIGIT = [
        1 => ['   ', '  |', '  |'],
        2 => [' _ ', ' _|', '|_ '],
        3 => [' _ ', ' _|', ' _|'],
        4 => ['   ', '|_|', '  |'],
        5 => [' _ ', '|_ ', ' _|'],
        6 => [' _ ', '|_ ', '|_|'],
        7 => [' _ ', '  |', '  |'],
        8 => [' _ ', '|_|', '|_|'], 
        9 => [' _ ', '|_|', ' _|']
    ];

    public function convert(int $input) :string
    {
        if ($this->width === 1 && $this->height === 1) {
            $digits = $this->splitNumber($input);
            return $this->concatenateLcdDigits($digits); 
        }
       //return "\n ___ \n    |\n ___|\n|     \n|___ ";  2 => '\n _ \n _|\n|_ ',
       $digits = $this->splitNumber($input);
       $defaultString = $this->concatenateLcdDigits($digits); 
       return $this->modifyDefaultString($defaultString);
    }

    private function modifyDefaultString($inputString): string{
        
        $modifiedString = "";
        $localWidth = $this->width;
    
        for($i = 0; $i < strlen($inputString); $i++) {
            if ($inputString[$i] === '_') {
                $modifiedString .= str_repeat('_', $localWidth);
               
            } else {
            $modifiedString .= $inputString[$i];
            }

        }
        // TODO add aditional lines dynamically.
        return $modifiedString;
    }

    private function splitNumber(int $input): array
    {
        return str_split($input);
    }

    private function concatenateLcdDigits($digits): string
    {
        $lineZero = "";
        $lineOne = "";
        $lineTwo = "";

        foreach ($digits as $digit) {
            $resultChars = $this->getCharsForDigit($digit);
            $lineZero .= $resultChars[0];
            $lineOne .= $resultChars[1];
            $lineTwo .= $resultChars[2];
        }

        return  "\n".$lineZero."\n".$lineOne."\n".$lineTwo;
    }

    private function getCharsForDigit(int $digit): array
    {

        return self::LCDCHARSOFDIGIT[$digit];
    }
}
