<?php

namespace Test;

use App\NumberToLcdConverter;
use PHPUnit\Framework\TestCase;

class NumberToLcdConverterTest extends TestCase
{

    /**
     * @test
     * @dataProvider provideTestDigitData
     */
    public function should_return_the_correct_chars_for_a_single_digit($expectedResult, $testDigit)
    {
        $converter = new NumberToLcdConverter();
        $result = $converter->convert($testDigit);
        self::assertSame($expectedResult, $result);
    }

    public static function provideTestDigitData()
    {
        return [
            "with input 1" => [
                "\n   \n  |\n  |",
                1,
            ],
            "with input 2" => [
                "\n _ \n _|\n|_ ",
                2,
            ],
            "with input 3" => [
                "\n _ \n _|\n _|",
                3,
            ],
            "with input 4" => [
                "\n   \n|_|\n  |",
                4,
            ],
            "with input 5" => [
                "\n _ \n|_ \n _|",
                5,
            ],
            "with input 6" => [
                "\n _ \n|_ \n|_|",
                6,
            ],
            "with input 7" => [
                "\n _ \n  |\n  |",
                7,
            ],
            "with input 8" => [
                "\n _ \n|_|\n|_|",
                8,
            ],
            "with input 9" => [
                "\n _ \n|_|\n _|",
                9,
            ]
        ];
    }

    /**
     * @test
     */
    public function should_convert_int_12_to_lcd_12()
    {
        # arrange 
        $testNumber = 12;
        $converter = new NumberToLcdConverter();
        $expectedString = "\n    _ \n  | _|\n  ||_ ";

        #act 
        $lcdString = $converter->convert($testNumber);

        # assert
        $this->assertEquals($expectedString, $lcdString);

    }

    /**
     * @test
     */
    public function should_convert_int_89_to_lcd_89()
    {
        # arrange 
        $testNumber = 89;
        $converter = new NumberToLcdConverter();
        $expectedString = "\n _  _ \n|_||_|\n|_| _|";

        #act 
        $lcdString = $converter->convert($testNumber);

        # assert
        $this->assertEquals($expectedString, $lcdString);

    }

    /**
     * @test
     */
    public function should_convert_int_1289_to_lcd_1289()
    {
        # arrange 
        $testNumber = 1289;
        $converter = new NumberToLcdConverter();
        $expectedString = "\n    _  _  _ \n  | _||_||_|\n  ||_ |_| _|";

        #act 
        $lcdString = $converter->convert($testNumber);

        # assert
        $this->assertEquals($expectedString, $lcdString);

    }

     /**
     * @test
     */
    public function should_convert_int_1_to_lcd_1_based_on_width_3_height_2()
    {
        # arrange 
        $testNumber = 2;
        $converter = new NumberToLcdConverter(3,2);
        $expectedString = "\n ___ \n    |\n ___|\n|     \n|___ ";
 
        #act 
        $lcdString = $converter->convert($testNumber);

        # assert
        $this->assertEquals($expectedString, $lcdString);

    }



}
