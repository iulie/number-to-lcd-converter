const ExampleClass = require('../../app/js/ExampleClass.js');

describe('ExampleClass', () => {
    test("should return true", () => {
        const exampleClass = new ExampleClass;

        expect(exampleClass.test()).toBe(true);
    });
    test('should fail', () => {
        const exampleClass = new ExampleClass;

        expect(exampleClass.test()).toBe(false);
    });
});
